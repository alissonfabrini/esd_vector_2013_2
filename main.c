#include <stdio.h>
#include <stdlib.h>
#include "vector.h"

void printv(Vector* v)
{
    int pos;
    
    printf("[");
    
    for (pos = 0; pos < v->size; pos++)
        printf("%d%s",
            v->data[pos],
            pos < v->size-1 ? ", " : "");
            
    printf("]\n");
}

void printe(Vector* v, unsigned int pos)
{
    if (pos < v->size)
        printf("v[%d] -> %d\n", pos, v->data[pos]);
}

void helper(void) {
    printf("uso: PROG [argumentos]\n"
           "\nonde os argumentos são:"
           "\n\t i n   : Insere o número 'n' na posição 'i' do vetor."
           "\n\t-i     : Remove o elemento de índice 'i' do vetor."
           "\n\t f n   : Busca o primeiro 'n' no vetor e imprime 'v[i] -> n'."
           "\n\t c n   : Imprime a quantidade de ocorrências de 'n' no vetor."
           "\n\t e i   : Imprime 'v[i] -> n'."
           "\n\t print : Imprime o vetor.\n");
}

int main(int argc, char** argv)
{
    if(argc < 2) {
        helper();
        return -1;
    }
        
    return 0;
}