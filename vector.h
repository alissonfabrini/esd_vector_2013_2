#ifndef VECTOR_H
#define VECTOR_H

#define MAX_VECTOR_SIZE 100

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

/* códigos de retorno */
enum {
    success = 0,
    error   = 100
};

typedef struct {
    int data[MAX_VECTOR_SIZE];
    int size;
} Vector;

void vector_init   (Vector* v);
int  vector_insert (Vector* v, int pos, int data);
int  vector_remove (Vector* v, int pos);
int  vector_find   (Vector* v, int data);
int  vector_count  (Vector* v, int data);

#endif